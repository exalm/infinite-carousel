
int main (string[] args) {
	var app = new Gtk.Application ("org.gnome.InfinitePaginator", ApplicationFlags.FLAGS_NONE);

	app.startup.connect (() => {
		Hdy.init ();
	});

	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new Infinitepaginator.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
